package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.volksbrett.VolksbrettApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = VolksbrettApplication.class)
@WebAppConfiguration
public class VolksbrettApplicationTests {

	@Test
	public void contextLoads() {
	}

}

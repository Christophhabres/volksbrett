package at.volksbrett;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import at.volksbrett.db.*;

@SpringBootApplication
public class VolksbrettApplication {

	public static void main(String[] args) {
		SpringApplication.run(VolksbrettApplication.class, args);

	}

	@Autowired
	private PinService pinService;

}

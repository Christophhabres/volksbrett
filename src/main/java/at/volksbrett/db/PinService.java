package at.volksbrett.db;

import java.util.List;

public interface PinService {
	Iterable<Pin> findOnly(String name);

	Iterable<Pin> findAll();

	Pin save(Pin entity);

	void delete(int id);

	Pin find(int id);

	Boolean exists(int id);

	void complete(int id);

	int getNextId();
	
	void update(Pin pin);
}

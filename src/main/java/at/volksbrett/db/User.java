package at.volksbrett.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class User {
	@Id
	@GeneratedValue
	private int id;

	@NotEmpty(message = "{validation.firstname.NotEmpty.message}")
	@Size(min = 2, max = 60, message = "{validation.firstname.Size.message}")
	private String name;

	@NotNull
	private String password;
	@SuppressWarnings("unused")
	private User() {
	}

	public User(String name,String password) {
		setName(name);
		setPassword(password);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

}

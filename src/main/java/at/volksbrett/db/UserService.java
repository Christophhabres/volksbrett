package at.volksbrett.db;

import java.util.List;




public interface UserService{
	
	Iterable<User> findAll();
	User save(User entity);
	void delete(int id);
	User find(int id);
}

	
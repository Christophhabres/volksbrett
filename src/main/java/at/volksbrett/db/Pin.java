package at.volksbrett.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Pin {
	@Id
	@GeneratedValue
	private int id;

	private String name;

	private String task;
	
	private Boolean comp;

	private String taskdesc;
	public String getTaskdesc() {
		return taskdesc;
	}
	public void setTaskdesc(String taskdesc) {
		this.taskdesc = taskdesc;
	}
	public Boolean getComp() {
		return comp;
	}
	public void setComp(Boolean comp) {
		this.comp = comp;
	}
	@SuppressWarnings("unused")
	private Pin() {
	}
	public Pin(String name,String task,Boolean comp, String taskdesc) {
		setName(name);
		setTask(task);
		setComp(comp);
		setTaskdesc(taskdesc);
	}
	public Pin(int id,String name,String task,Boolean comp, String taskdesc) {
		setName(name);
		setTask(task);
		setComp(comp);
		setTaskdesc(taskdesc);
		setId(id);
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}
}

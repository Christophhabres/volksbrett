package at.volksbrett.db;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("pinService")
public class PinServiceImpl implements PinService {
	// private static final Logger log =
	// LoggerFactory.getLogger(UserServiceImpl.class);
	private PinRepository pinRepository;

	@Autowired
	public PinServiceImpl(PinRepository pinRepository2) {
		this.pinRepository = pinRepository2;
	}

	@Override
	public Iterable<Pin> findAll() {
		return pinRepository.findAll();
	}

	@Override
	public Pin save(Pin entity) {
		return pinRepository.save(entity);
	}

	@Override
	public void delete(int id) {
		pinRepository.delete(id);
	}

	@Override
	public Pin find(int id) {
		return pinRepository.findOne(id);
	}

	@Override
	public Iterable<Pin> findOnly(String name) {
		// TODO Auto-generated method stub
		List<Pin> pins = new ArrayList<Pin>();
		for (Pin p : pinRepository.findAll()) {
			if (p.getName().equals(name))
				pins.add(p);
		}
		return pins;
	}

	@Override
	public Boolean exists(int id) {
		for (Pin p : pinRepository.findAll()) {
			if (p.getId() == id)
				return true;
		}
		return false;
	}

	@Override
	public void complete(int id) {
		// TODO Auto-generated method stub
		for (Pin p : pinRepository.findAll()) {
			if (p.getId() == id) {
				p.setComp(!p.getComp());
				pinRepository.save(p);
				break;
			}

		}
	}

	@Override
	public int getNextId() {
		int i = 0;
		for (Pin p : pinRepository.findAll()) {
			if (i < p.getId())
				i = p.getId();
		}
		return i + 1;
	}

	@Override
	public void update(Pin pin) {
		for (Pin p : pinRepository.findAll()) {
			if (p.getId() == pin.getId()) {
				p.setTask(pin.getTask());
				p.setTaskdesc(pin.getTaskdesc());
				pinRepository.save(p);
				break;
			}

		}
	}

}

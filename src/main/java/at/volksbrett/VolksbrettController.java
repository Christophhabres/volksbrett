package at.volksbrett;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.http.HttpStatus;

import at.volksbrett.db.*;

@Controller
public class VolksbrettController {

	private PinService pinService;

	@Autowired
	public VolksbrettController(PinService pinService) {

		this.pinService = pinService;
	}

	// @InitBinder
	// public void binder(WebDataBinder binder) {
	// binder.registerCustomEditor(java.sql.Date.class, new Umwandeln());
	// }

	// final class Umwandeln extends PropertyEditorSupport {
	//
	// @Override
	// public void setAsText(String text) throws IllegalArgumentException {
	// DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	// LocalDate d = LocalDate.parse(text, fmt);
	// setValue(java.sql.Date.valueOf(d));
	// }
	// }
	@RequestMapping(value = "/login")
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/logout")
	public String logout() {
		return "logout";
	}

	// @RequestMapping(value = "/login", method = RequestMethod.GET)
	// public ModelAndView login() {
	// ModelAndView mav = new ModelAndView("login");
	// return mav;
	// }
	//
	// @RequestMapping(value = "/login.do", method = RequestMethod.POST)
	// public ModelAndView checkLogin(@ModelAttribute("user") User user,
	// BindingResult result) {
	// for(User u: userService.findAll()){
	// if(u.getName()==user.getName()){
	// if(u.getPassword()==user.getPassword())
	// mav = new ModelAndView("next");
	// }
	// System.out.println(u.getName());
	// }
	// ModelAndView mav = new ModelAndView("login");
	// return mav;
	// }

	@RequestMapping(value = "/index")
	public ModelAndView index() {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			Spliterator<Pin> it = pinService
					.findOnly(userDetails.getUsername()).spliterator();
			ModelAndView mav = new ModelAndView("index");
			mav.addObject("users", StreamSupport.stream(it, false).toArray());
			mav.addObject("user", userDetails.getUsername());
			return mav;
		} catch (Exception e) {
			return new ModelAndView("login");
		}
	}

	@RequestMapping(value = "/about")
	public ModelAndView about() {
		ModelAndView mav = new ModelAndView("about");
		user(mav);
		return mav;
	}

	@RequestMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView mav = new ModelAndView("add");
		user(mav);
		return mav;
	}

	private void user(ModelAndView mav) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		mav.addObject("user", userDetails.getUsername());
	}

	@RequestMapping(value = "/add.new", method = RequestMethod.POST)
	public ModelAndView addnew(@ModelAttribute("pinform") @Valid Pin pin,
			BindingResult result) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		pinService.save(new Pin(userDetails.getUsername(), pin.getTask(),
				false, pin.getTaskdesc()));
		return index();
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(
			@org.springframework.web.bind.annotation.PathVariable int id) {
		ModelAndView mav = new ModelAndView("edit");
		user(mav);
		mav.addObject("task", pinService.find(id).getTask());
		mav.addObject("desc", pinService.find(id).getTaskdesc());
		mav.addObject("id", id);

		return mav;
	}

	@RequestMapping(value = "/edit.do", method = RequestMethod.POST)
	public ModelAndView editdo(@ModelAttribute("editform") @Valid Pin pin,
			BindingResult result) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		pinService.update(pin);
		return index();
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(
			@org.springframework.web.bind.annotation.PathVariable int id) {
		if (pinService.exists(id))
			pinService.delete(id);
		return index();
	}

	@RequestMapping(value = "/complete/{id}", method = RequestMethod.GET)
	public ModelAndView complete(
			@org.springframework.web.bind.annotation.PathVariable int id) {
		if (pinService.exists(id))
			pinService.complete(id);
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return index();
	}
}
